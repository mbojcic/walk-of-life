<?php

class Data{
    function __construct($iterations, $species, $cells, $organisms)
    {
        $this->iterations = $iterations;
        $this->species = $species;
        $this->cells = $cells;
        $this->organisms = $organisms;
    }

    public $iterations;
    public $species;
    public $cells;
    public $organisms;
}

class XmlParser
{
    public static function readXMLDocument($file_name){
        if (file_exists($file_name)) {
            $xml = simplexml_load_file($file_name);
            $organisms = array();
            foreach($xml->organisms->organism as $organism)
                $organisms[current($organism->x_pos)][current($organism->y_pos)] =new Cell(
                    current($organism->x_pos),
                    current($organism->y_pos),
                    current($organism->species)
                );

            $initial_world_object = new Data(
                current($xml->world->iterations),
                current($xml->world->species),
                current($xml->world-> cells),
                $organisms
            );

            return $initial_world_object;
        }
        else
        {
            exit('Failed to open file');
        }
    }

    public static function createXMLDocument( $data){

        $xml = new DOMDocument('1.0', 'utf-8');

        $xml->formatOutput = true;

        $xml_life = $xml->createElement('life');
        $xml ->appendChild($xml_life);
        $xml_world = $xml->createElement('world');
        $xml_life -> appendChild( $xml_world );
        $xml_cells = $xml->createElement('cells', $data->cells);
        $xml_cells -> appendChild($xml->createTextNode(''));
        $xml_world -> appendChild($xml_cells );
        $xml_species = $xml->createElement('species', $data->species);
        $xml_species -> appendChild($xml->createTextNode(''));
        $xml_world -> appendChild($xml_species );
        $xml_iterations = $xml->createElement('iterations', $data->iterations);
        $xml_iterations -> appendChild($xml->createTextNode(''));
        $xml_world      -> appendChild($xml_iterations );
        $xml_organisms      = $xml->createElement('organisms');
        foreach($data->organisms as $organism) {
            $xml_life->appendChild($xml_organisms);
            $xml_x_pos = $xml->createElement('x_pos', $organism->x());
            $xml_x_pos->appendChild($xml->createTextNode(''));
            $xml_organisms->appendChild($xml_x_pos);
            $xml_y_pos = $xml->createElement('y_pos', $organism->y());
            $xml_y_pos->appendChild($xml->createTextNode(''));
            $xml_organisms->appendChild($xml_y_pos);
            $xml_spec = $xml->createElement('species', $organism->getType());
            $xml_spec->appendChild($xml->createTextNode(''));
            $xml_organisms->appendChild($xml_spec);
        }

        return $xml;
    }
}