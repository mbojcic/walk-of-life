<?php

require_once('Cell.php');

class EvolutionEvaluator{

    public static function evaluateLifeEvolutionOnSpecificCell($cell, $matrix){
        $aliveNeighbors = EvolutionEvaluator::orderAndCountAliveNeighborsByType(EvolutionEvaluator::neighborhood($cell), $matrix);
        $neighborsWithPopulationEqualToThreeTypesHashMap = array_filter($aliveNeighbors, function($count){return $count === 3; });

        if(!empty($cell->getType()) && (empty($aliveNeighbors[$cell->getType()]) || $aliveNeighbors[$cell->getType()] > 3 || $aliveNeighbors[$cell->getType()] < 2))
        {
            return 0;
        }
        elseif(!empty($cell->getType() && !empty($neighborsWithPopulationHigherThenThreeTypesHashMap)))
        {
            return array_rand($neighborsWithPopulationEqualToThreeTypesHashMap, 1);
        }
        else
        {
            return $cell->getType();
        }

    }

    private static function orderAndCountAliveNeighborsByType($neighbors, $matrix)
    {
        $aliveNeighborhood = array();
        foreach ($neighbors as $neighbour)
        {
            if(array_key_exists($neighbour->x(), $matrix) && array_key_exists($neighbour->y(), $matrix[$neighbour->x()]) && !empty($matrix[$neighbour->x()][$neighbour->y()]->getType()))
            {
                $neighbourOrganism = $matrix[$neighbour->x()][$neighbour->y()];
                if(isset($aliveNeighborhood[$neighbourOrganism->getType()]) && !empty($aliveNeighborhood[$neighbourOrganism->getType()]))
                    $aliveNeighborhood[$neighbourOrganism->getType()] += 1;
                else
                    $aliveNeighborhood[$neighbourOrganism->getType()] = 1;
            }
        }
        return $aliveNeighborhood;
    }

    private static function neighborhood($cell)
    {
        $right = $cell->x() + 1;
        $left = $cell->x() - 1;
        $top = $cell->y() + 1;
        $bottom = $cell->y() - 1;
        return array(
            new Cell($cell->x(), $top, 0),
            new Cell($right, $top, 0),
            new Cell($right, $cell->y(), 0),
            new Cell($right, $bottom, 0),
            new Cell($cell->x(), $bottom, 0),
            new Cell($left, $bottom, 0),
            new Cell($left, $cell->y(), 0),
            new Cell($left, $top, 0),
        );
    }


}