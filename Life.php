<?php
error_reporting(E_ALL);

require_once('XMLDocumentHelper.php');
require_once('World.php');

$initial_data = XmlParser::readXMLDocument('in.xml');
$world = new World($initial_data->cells, $initial_data->organisms);

for($iteration =0; $iteration < $initial_data->iterations; $iteration++ ) {
    $output = "Iteration: {$world->getTurnCount()}";
    $output .= "\n".render($world, $initial_data);
    $world->walk();
    system('clear');
    echo $output;
    sleep(1);
}

$newWorldData = worldToWorldData($world, $initial_data);

$outputXmlDocument = XmlParser::createXMLDocument($newWorldData);
$dom = new DOMDocument('1.0');
$outputXmlDocument->save("out.xml");


function worldToWorldData($world, $initial_data)
{
    $cellsArray = array();


    foreach($world->getCells() as $currentCellRow)
    {
        foreach($currentCellRow as $currentCell)
        {
            if(!empty($currentCell->getType())){
                $cellsArray[] = new Cell($currentCell->x(), $currentCell->y(), $currentCell->getType());
            }
        }
    }

    $worldData = new Data($initial_data->iterations, $initial_data->species, $initial_data->cells, $cellsArray);

    return $worldData;
}

function render($world, $initial_data) {
    $rendering = '';
    for ($y = 0; $y <= $initial_data->cells -1; $y++) {
        $rendering .= $y;
        if($y <= 9){ $rendering .= " ";}
        for ($x = 0; $x <= $initial_data->cells -1; $x++) {
            $cells = $world->getCells();
            $rendering .= '|' . (isset($cells[$x][$y]) ? (($cells[$x][$y]->getType() != 0) ? "{$cells[$x][$y]->getType()}" : " ") : '|');
        }
        $rendering .= "\n";
    }
    return $rendering;
}