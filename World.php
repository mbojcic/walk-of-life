<?php

require_once('Cell.php');
require_once('EvolutionEvaluator.php');

class World {

    private $turn;
    private $matrix;
    private $newMatrix;

    public function __construct($noOfCells, $organisms)
    {
        $this->turn = 0;
        $this->setupWorld($noOfCells, $organisms);

    }

    private function setupWorld($noOfCells, $organisms)
    {
        $this->matrix = array();
        for($x=0; $x < $noOfCells; $x++)
        {
            for($y=0; $y < $noOfCells; $y++)
            {
                if(array_key_exists($x, $organisms) && array_key_exists($y, $organisms[$x]))
                    $this->matrix[$x][$y] = new Cell($x, $y, $organisms[$x][$y]->getType());
                else
                    $this->matrix[$x][$y] = new Cell($x, $y, 0);
            }
        }
    }

    function walk()
    {

        foreach($this->matrix as $currentCellRow)
        {
            foreach($currentCellRow as $currentCell)
            {
                $type = EvolutionEvaluator::evaluateLifeEvolutionOnSpecificCell($currentCell, $this->matrix);
                $this->newMatrix[$currentCell->x()][$currentCell->y()] = new Cell($currentCell->x(), $currentCell->y(), $type);
            }
        }

        unset($this->matrix);

        $this->matrix = $this->newMatrix;

        $this->turn += 1;
    }

    public function getTurnCount()
    {
        return $this->turn;
    }

    function getCells()
    {
        return $this->matrix;
    }
}

